from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.

def receipt_list(request):
  receipt_lists = Receipt.objects.all()
  context = {
    "receipt": receipt_lists
  }
  return render(request, "receipts/list.html", context)
