from django.urls import path, include
from receipts.views import receipt_list
from django.contrib import admin


urlpatterns = [
    path("", receipt_list, name="home"),
]
